package com.douglassdigothy2

import android.graphics.*
import java.lang.Math.PI
import kotlin.math.cos
import kotlin.math.sin

class Bullet(xPos: Float, yPos: Float, angle: Float) : Entity() {
    var bulletPath: Path = Path()
    var bulletRegion: Region = Region()
    var bulletWidth: Float = 32f
    var bulletHeight: Float = 5f
    var moveMat: Matrix = Matrix()
    private var xOffset: Float = 0f
    private var yOffset: Float = 0f
    private val paint = Paint().apply {
        color = Color.WHITE
        strokeWidth = 2F
        isAntiAlias = true
    }

    init {
        this.xPos = xPos
        this.yPos = yPos

        //movement speed for bullets
        moveSpeed = 46

        bulletPath.moveTo(xPos,yPos + (bulletHeight/2))
        bulletPath.lineTo(xPos + bulletWidth, yPos + (bulletHeight/2))
        bulletPath.lineTo(xPos + bulletWidth, yPos - (bulletHeight/2))
        bulletPath.lineTo(xPos, yPos - (bulletHeight/2))
        bulletPath.close()

        bulletRegion.setPath(bulletPath, regionClip)

        //make movement matrix
        xOffset = moveSpeed * cos(angle)
        yOffset = moveSpeed * sin(angle)
        moveMat.setTranslate(xOffset, yOffset)

        //rotate rectangle
        val rotMat = Matrix()
        rotMat.setRotate(angle*180/PI.toFloat(), xPos, yPos)
        rotMat.postConcat(moveMat)
        bulletPath.transform(rotMat)
    }

    override fun draw(canvas: Canvas)
    {
        canvas.drawPath(bulletPath, paint)
    }

    // moveX and moveY do nothing
    override fun update(moveX: Float, moveY: Float) {
        bulletPath.transform(moveMat)
        bulletRegion.setPath(bulletPath, regionClip)

        xPos += xOffset
        yPos += yOffset
    }

    fun onScreen() : Boolean {
        return (0 < xPos) && (xPos < screenWidth) && (0 < yPos) && (yPos < screenHeight)
    }

}