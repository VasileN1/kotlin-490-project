package com.douglassdigothy2

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // remove notification bar
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        //setContentView(R.layout.activity_main)
    }

    override fun onRestart() {
        super.onRestart()
        val intent = Intent(this, PauseActivity::class.java)
        startActivity(intent)
    }

    override fun onPause() {
        super.onPause()
    }
}