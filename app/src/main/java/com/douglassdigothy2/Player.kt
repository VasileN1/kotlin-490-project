package com.douglassdigothy2

import android.graphics.*
import android.graphics.Region
import android.util.Log

class Player : Entity() {
    val paint = Paint().apply {
        color = Color.WHITE
        strokeWidth = 2F
        isAntiAlias = true
    }

    // Applying momentum to player's movement
    private var prevMoveX : Float = 0f
    private var prevMoveY : Float = 0f
    private var drag : Float = 0.1f
    private var moveAccel : Int = 6

    // Angles of the gun
    var shootAngle: Float = 0f
    // Movement Angles
    var currentAngle: Float = (Math.PI/2).toFloat()
    var desiredAngle: Float = 0f

    // Defining the player's shape/position on the screen
    var playerPath: Path = Path()
    var playerRegion: Region = Region()
    private var topCoord: Coord = Coord()
    private var botRightCoord: Coord = Coord()
    private var botCenterCoord: Coord = Coord()
    private var botLeftCoord: Coord = Coord()
    private val halfWidth = 66 / 2
    private val halfHeight = 76 / 2

    init {
        xPos = screenWidth/2F
        yPos = screenHeight/2F

        topCoord = Coord(xPos, yPos + halfHeight)
        botRightCoord = Coord(xPos + halfWidth, yPos - halfHeight)
        botCenterCoord = Coord(xPos, yPos - halfHeight/3)
        botLeftCoord = Coord(xPos - halfWidth, yPos - halfHeight)

        // creating player path
        playerPath = Path()
        playerPath.moveTo(topCoord.x, topCoord.y) // Top of the player
        playerPath.lineTo(botRightCoord.x, botRightCoord.y)
        playerPath.lineTo(botCenterCoord.x, botCenterCoord.y)
        playerPath.lineTo(botLeftCoord.x, botLeftCoord.y)
        playerPath.close()

        playerRegion.setPath(playerPath, regionClip)
    }

    /**
     * Draws the object on to the canvas.
     * bitmap can be this.bitmap
     */
    override fun draw(canvas: Canvas)
    {
        // Draw out player path
        paint.strokeWidth = 5f
        //paint.color = Color.WHITE
        canvas.drawPath(playerPath, paint)

        // Draw base of turret
        paint.color = Color.GRAY
        canvas.drawCircle(xPos, yPos, 10F, paint)

        paint.strokeWidth = 5f
        val gunEnd = getGunRotation()
        canvas.drawLine(xPos, yPos, gunEnd.first, gunEnd.second, paint)
    }

    /**
     * update properties for the game object
     */
    override fun update(movementX: Float, movementY: Float)
    {
        // Check if we're on the screen
        if(xPos > screenWidth + 75)
        {
            playerPath.transform(Matrix().apply { setTranslate(-(screenWidth + 75F), 0F) })
            xPos -= (screenWidth + 75)
        }
        else if(xPos < 0 - 75)
        {
            playerPath.transform(Matrix().apply { setTranslate(screenWidth + 75F, 0F) })
            xPos += (screenWidth + 75)
        }

        if(yPos > screenHeight + 75)
        {
            playerPath.transform(Matrix().apply { setTranslate(0F, -(screenHeight + 75F)) })
            yPos -= (screenHeight + 75)
        }
        else if(yPos < 0 - 75)
        {
            playerPath.transform(Matrix().apply { setTranslate(0F, screenHeight + 75F) })
            yPos += (screenHeight + 75)
        }

        // wind resistance to slow down ship and momentum
        var moveX = movementX
        var moveY = movementY
        moveX += prevMoveX
        moveX -= drag * moveX
        moveY += prevMoveY
        moveY -= drag * moveY
        prevMoveX = moveX
        prevMoveY = moveY

        // Final offsets
        val moveOffsetX = (moveX * moveAccel)
        val moveOffsetY = (moveY * moveAccel)


        // Calculate our desired angle
        //desiredAngle = Math.atan2(moveX.toDouble(),moveY.toDouble()).toFloat()
        //Log.d("Sad",desiredAngle.toString())

        // Calculate the difference from our current angle
        val differenceAngle: Float = desiredAngle - currentAngle

        // Prepare rotation and translation matrix
        val rotMatrix = Matrix().apply { setRotate(Math.toDegrees(differenceAngle.toDouble()).toFloat(), xPos, yPos) }
        val moveMatrix = Matrix().apply { setTranslate(moveOffsetX,moveOffsetY) }

        // update with new coordinates and rotation
        playerPath.transform(rotMatrix)
        playerPath.transform(moveMatrix)

        // Move the center of the player
        xPos += moveOffsetX
        yPos += moveOffsetY

        // update our region
        playerRegion.setPath(playerPath, regionClip)

        // Our new "old" angle is our current one
        currentAngle = desiredAngle
    }

    private fun getGunRotation() : Pair<Float, Float>
    {
        val xEnd = 40.0 * Math.cos(shootAngle.toDouble()) + xPos
        val yEnd = 40.0 * Math.sin(shootAngle.toDouble()) + yPos
        return Pair(xEnd.toFloat(), yEnd.toFloat())
    }
}