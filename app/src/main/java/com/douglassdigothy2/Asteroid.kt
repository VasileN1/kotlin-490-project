package com.douglassdigothy2

import android.graphics.*
import kotlin.math.max
import kotlin.random.Random

class Asteroid(val speedModifier: Float = 1F, val maxRadius: Int = 150, val minRadius: Int = 35): Entity()
{
    private val paint = Paint().apply {color = Color.WHITE; strokeWidth = 2F; isAntiAlias = true; style = Paint.Style.STROKE}
    var asteroidPath: Path = Path()
    var asteroidRegion: Region = Region()

    // Movement Offsets for each axis
    var moveOffsetX: Float = 0f
    var moveOffsetY: Float = 0f

    // List of 8 x/y coords to be connected with a line
    private var asteroidVertices: MutableList<Coord> = mutableListOf()

    // how many radians per draw do we rotate?
    private val rotation = Random.nextFloat() * 2 - 1


    init {
        // Generates asteroids on the edges of the screen randomly
        val side = Random.nextInt(0,3)
        when (side)
        {
            0 -> { // top of screen
                xPos = Random.nextFloat() * screenWidth
                yPos = 0F - maxRadius
            }
            1 -> { // right
                xPos = screenWidth + maxRadius.toFloat()
                yPos = Random.nextFloat() * screenHeight
            }
            2 -> { // bottom
                xPos = Random.nextFloat() * screenWidth
                yPos = screenHeight + maxRadius.toFloat()
            }
            3 -> { // left
                xPos = 0 - maxRadius.toFloat()
                yPos = Random.nextFloat() * screenHeight
            }
        }

        // Generate a x and y offset such that it isn't close to 0
        // number between (1 and 5) times our speedModifier
        moveOffsetX = (Random.nextFloat() * 4 + 1F) * speedModifier
        moveOffsetY = (Random.nextFloat() * 4 + 1F) * speedModifier

        val sign = intArrayOf(-1,1)

        // Now a number between -5, -1 and 1, 5
        moveOffsetX *= sign[Random.nextInt(0, 2)]
        moveOffsetY *= sign[Random.nextInt(0, 2)]

        var angle: Double = 0.0 // Angle is in rads

        // generate 8 points around xPos and yPos at varying distances seperated by 45 degrees
        for (i in 0 until 8)
        {
            // random distance from xPos/Y. Length acts as the HYPOTENUSE
            val length = Random.nextInt(minRadius,maxRadius)

            // get the coords
            val endX = ((Math.sin(angle) * length) + xPos).toFloat()
            val endY = ((Math.cos(angle) * length) + yPos).toFloat()

            // add to our asteroidVertices list
            asteroidVertices.add(Coord(endX,endY))

            // Change the starting shootAngle for our next line
            angle += 45 * (Math.PI/180)
        }

        // SET ASTEROID PATH
        asteroidPath = Path()
        asteroidPath.moveTo(asteroidVertices[0].x, asteroidVertices[0].y) // Set starting position

        // skipping first vertex!
        for(i in 1 until asteroidVertices.size)
        {
            asteroidPath.lineTo(asteroidVertices[i].x, asteroidVertices[i].y)
        }

        // connecting to the final point
        asteroidPath.close()

    }

    override fun draw(canvas: Canvas)
    {
        canvas.drawPath(asteroidPath, paint)
    }

    override fun update(moveX: Float, moveY: Float)
    {
        if (moveX != 0F && moveY != 0F)
        {
            moveOffsetX = moveX
            moveOffsetY = moveY
        }

        var xMove = 0F
        var yMove = 0F
        val transMat = Matrix() // points to an object, can be val

        // When we hit the right edge
        if (xPos > screenWidth + maxRadius)
        {
            // Move the center
            xMove = -screenWidth - maxRadius * 2F
            xPos += xMove

            // Move the path by the same amount
            transMat.setTranslate(xMove, 0F)
            asteroidPath.transform(transMat)
        }
        // When we hit the left edge
        else if (xPos < 0 - maxRadius)
        {
            xMove = screenWidth + maxRadius * 2F
            xPos += xMove

            transMat.setTranslate(xMove, 0F)
            asteroidPath.transform(transMat)
        }
        // When we hit the top of the screen
        if (yPos > screenHeight + maxRadius)
        {
            yMove = -screenHeight - maxRadius * 2F
            yPos += yMove

            transMat.setTranslate(0F, yMove)
            asteroidPath.transform(transMat)
        }
        // When we hit the bottom of the screen
        else if (yPos < 0 - maxRadius)
        {
            yMove = screenHeight + maxRadius * 2F
            yPos += yMove

            transMat.setTranslate(0F, yMove)
            asteroidPath.transform(transMat)
        }

        val moveMatrix: Matrix = Matrix().apply { setTranslate(moveOffsetX, moveOffsetY) }
        val rotMatrix: Matrix = Matrix().apply { setRotate(rotation, xPos, yPos) }

        // update our asteroid's position on screen
        xPos += moveOffsetX
        yPos += moveOffsetY

        asteroidPath.transform(moveMatrix)
        asteroidPath.transform(rotMatrix)

        // update the asteroids region for collision
        asteroidRegion.setPath(asteroidPath, regionClip)

    }
}