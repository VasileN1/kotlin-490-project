package com.douglassdigothy2

import android.graphics.Color
import android.graphics.Paint
import kotlin.math.roundToInt

data class Line(var startCoord: Coord, var endCoord: Coord, val paint: Paint =  Paint().apply {
                                                                                color = Color.WHITE
                                                                                strokeWidth = 2F
                                                                                isAntiAlias = true })
{
    // Distance between two points
    private fun distance(coord1: Coord, coord2: Coord): Double {
        return (Math.sqrt(Math.pow((coord1.x - coord2.x).toDouble(), 2.0) + Math.pow((coord1.y - coord2.y).toDouble(), 2.0)))
    }
}

data class Coord(var x: Float = 0F, var y: Float = 0F)
{
    override fun toString(): String
    {
        return "x: $x, y: $y"
    }
}


