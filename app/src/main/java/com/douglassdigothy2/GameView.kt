package com.douglassdigothy2

import android.content.Context
import android.content.res.Resources
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceView
import android.view.SurfaceHolder
import kotlin.math.*
import android.graphics.*
import android.util.Log


/**
 * GameView is our playground.
 */
class GameView(context: Context, attributes: AttributeSet) : SurfaceView(context, attributes), SurfaceHolder.Callback {
    private val thread: GameThread
    // Size of the screen
    val screenWidth: Int = Resources.getSystem().displayMetrics.widthPixels
    val screenHeight: Int = Resources.getSystem().displayMetrics.heightPixels

    // Id of touch action
    private var touched: Boolean = false
    var moveActionId: Int = -1
    var gunActionId: Int = -1

    // Position of the movement joystick center
    val leftJoyX: Int = screenWidth/4
    val leftJoyY: Int = (screenHeight * 3)/4

    // Position of the aim joystick
    val rightJoyX: Int = (screenWidth * 3)/4
    val rightJoyY: Int = (screenHeight * 3)/4

    // Player Object and it's inputs
    private var player = Player()
    private var moveInputX: Float = 0f
    private var moveInputY: Float = 0f
    private var iFrames: Int = 0

    // Bullet Objects and their inputs
    private var bullets : MutableList<Bullet> = mutableListOf()
    private var shootCountDown: Int = -1
    private var shootPeriod: Int = 8

    // Asteroid Objects.
    private var asteroids = mutableListOf(Asteroid(),Asteroid(),Asteroid(),Asteroid(),Asteroid(),Asteroid(),Asteroid())
    private var asteroidSpeedMod = 1.01F
    private var score = 0

    // for printing
    var toPrint: String = ""


    init {
        // add callback
        holder.addCallback(this)

        // instantiate the game thread
        thread = GameThread(holder, this)
    }

    override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
        // game objects
        player = Player()

        // start the game thread
        thread.setRunning(true)
        thread.start()
    }

    override fun surfaceChanged(surfaceHolder: SurfaceHolder, i: Int, i1: Int, i2: Int) {

    }

    override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {
        var retry = true
        while (retry) {
            try {
                thread.setRunning(false)
                thread.join()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            retry = false
        }
    }

    /**
     * Function to update the positions of sprites
     */
    fun update() {
        val asteroidsToRemove = mutableListOf<Asteroid>()
        val bulletsToRemove = mutableListOf<Bullet>()

        if(iFrames != 0)
            iFrames--
        else
            player.paint.color = Color.WHITE
        if(iFrames < 60 && iFrames != 0)
            player.paint.color = Color.YELLOW
        else if(iFrames >= 60)
            player.paint.color = Color.RED

        // Check for collisions before anything else!
        for(asteroid in asteroids)
        {
            var tempRegion = Region(asteroid.asteroidRegion)
            tempRegion.op(player.playerRegion, Region.Op.INTERSECT)

            if(!tempRegion.isEmpty)
            {
                if(iFrames == 0)
                {
                    val transMat = Matrix().apply { setTranslate((screenWidth/2F)-(player.xPos), (screenHeight/2F)-(player.yPos)) }

                    player.xPos = screenWidth/2F
                    player.yPos = screenHeight/2F
                    player.playerPath.transform(transMat)
                    player.paint.color = Color.RED

                    iFrames = 180
                    score -= 25
                    if(score < 0)
                        score = 0
                }
                else{
                    asteroidsToRemove.add(asteroid)
                    score++
                }
            }

            for(bullet in bullets)
            {
                tempRegion = Region(asteroid.asteroidRegion)
                tempRegion.op(bullet.bulletRegion, Region.Op.INTERSECT)

                if (!tempRegion.isEmpty)
                {
                    if(!asteroidsToRemove.contains(asteroid))
                        asteroidsToRemove.add(asteroid)
                    bulletsToRemove.add(bullet)
                    score++
                }
            }
        }

        // repopulate asteroids
        for (i in 0 until asteroidsToRemove.size)
        {
            asteroids.add(Asteroid(speedModifier = asteroidSpeedMod))
            asteroidSpeedMod = (0.02F * score) + 1
        }
        toPrint = "Score: $score"

        bullets.removeAll(bulletsToRemove)
        asteroids.removeAll(asteroidsToRemove)

        // update the positions of all game objects
        player.update(moveInputX, moveInputY)
        asteroids.forEach { it.update() }
        bullets.forEach { it.update() }
        bullets = bullets.filter { it.onScreen() }.toMutableList()

        //shoot and keep track of time
        if (shootCountDown == 0)
        {
            bullets.add(Bullet(player.xPos, player.yPos, player.shootAngle ))
            shootCountDown = shootPeriod
        }
        else if (shootCountDown > 0)
        {
            shootCountDown--
        }
    }

    /**
     * Everything that has to be drawn on Canvas
     */
    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        // Draw on screen Objects
        player.draw(canvas)
        for(asteroid in asteroids)
        {
            asteroid.draw(canvas)
        }
        for(bullet in bullets) {
            bullet.draw(canvas)
        }

        val paint = Paint()
        paint.color = Color.WHITE
        paint.textSize = 50f

        canvas.drawText(toPrint, 70f, 70f, paint)

        // Draw joysticks
        paint.color = Color.DKGRAY
        canvas.drawCircle(leftJoyX.toFloat(),leftJoyY.toFloat(),20f, paint)
        canvas.drawCircle(rightJoyX.toFloat(),rightJoyY.toFloat(),20f, paint)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val pointerIndex = event.actionIndex
        val pointerId = event.getPointerId(pointerIndex)

        when (event.actionMasked) {
            // when you first hit the screen
            MotionEvent.ACTION_DOWN, MotionEvent.ACTION_POINTER_DOWN -> {
                if(event.getX(pointerIndex) <  screenWidth/2)
                {
                    calculateMoveInput(event.getX(pointerIndex), event.getY(pointerIndex))
                    moveActionId = pointerId
                }
                else
                {
                    player.shootAngle = atan2(event.getY(pointerIndex) - rightJoyY, event.getX(pointerIndex) - rightJoyX)
                    gunActionId = pointerId

                    //set gun to full auto
                    shootCountDown = 0
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if(moveActionId != -1)
                {
                    val actionIndex = event.findPointerIndex(moveActionId)
                    calculateMoveInput(event.getX(actionIndex), event.getY(actionIndex))
                }
                if((gunActionId != -1))
                {
                    val actionIndex = event.findPointerIndex(gunActionId)
                    player.shootAngle = atan2(event.getY(actionIndex) - rightJoyY, event.getX(actionIndex) - rightJoyX)
                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_POINTER_UP -> {
                moveInputY = 0f
                moveInputX = 0f
                if(pointerId == moveActionId)
                    moveActionId = -1
                if(pointerId == gunActionId) {
                    gunActionId = -1

                    //turn off gun
                    shootCountDown = -1
                }
            }
            MotionEvent.ACTION_CANCEL -> false
            MotionEvent.ACTION_OUTSIDE -> false
        }
        return true
    }

    /**
     * calculates the joystick input from screen input and stores it in moveInputX, moveInputY, and player.desiredAngle
     * @param x the pointer location event.getX(event.actionIndex)
     * @param y same thing as x but the y version
     */
    fun calculateMoveInput(x: Float, y: Float) {
        player.desiredAngle = Math.atan2((y - leftJoyY).toDouble(), (x - leftJoyX).toDouble()).toFloat()
        //distance from center of joystick squared
        val magnitude: Float = ((((x - leftJoyX) * (x - leftJoyX)) + ((y - leftJoyY) * (y - leftJoyY))) / (9 * (screenHeight - leftJoyY) * (screenHeight - leftJoyY) / 16)).coerceIn(-1f,1f)
        moveInputX = cos(player.desiredAngle) * magnitude
        moveInputY = sin(player.desiredAngle) * magnitude
        //` = magnitude.toString()
    }

}