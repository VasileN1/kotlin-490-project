package com.douglassdigothy2

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Button

class PauseActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pause_activity)
    }

    override fun onRestart() {
        super.onRestart()
        setContentView(R.layout.pause_activity)
    }

    override fun onResume() {
        super.onResume()
        setContentView(R.layout.pause_activity)
    }

    // button functions
    fun onGameRestart(view: View){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun onGameResume(view: View){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}