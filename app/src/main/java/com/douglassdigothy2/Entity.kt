package com.douglassdigothy2

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Region

abstract class Entity
{
    var xPos: Float = 0F
    var yPos: Float = 0F
    var moveSpeed: Int = 0
    protected val screenWidth = Resources.getSystem().displayMetrics.widthPixels
    protected val screenHeight = Resources.getSystem().displayMetrics.heightPixels
    protected val regionClip: Region = Region(0,0, screenWidth, screenHeight)

    abstract fun draw(canvas: Canvas)

    abstract fun update(moveX: Float = 0F, moveY: Float = 0F)
}